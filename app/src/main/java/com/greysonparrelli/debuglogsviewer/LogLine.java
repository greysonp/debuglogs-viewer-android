package com.greysonparrelli.debuglogsviewer;

import androidx.annotation.NonNull;

public class LogLine {

  private final String   body;
  private final LogLevel logLevel;

  public LogLine(@NonNull String body, @NonNull LogLevel logLevel) {
    this.body     = body;
    this.logLevel = logLevel;
  }

  public @NonNull String getBody() {
    return body;
  }

  public @NonNull LogLevel getLogLevel() {
    return logLevel;
  }

  public enum LogLevel {
    NONE, VERBOSE, DEBUG, INFO, WARNING, ERROR, ASSERT
  }
}
