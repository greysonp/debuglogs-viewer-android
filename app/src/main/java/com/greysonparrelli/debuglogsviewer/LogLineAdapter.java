package com.greysonparrelli.debuglogsviewer;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class LogLineAdapter extends RecyclerView.Adapter<LogLineAdapter.LineViewHolder> {

  private final List<LogLine> lines         = new ArrayList<>();
  private final ScrollManager scrollManager = new ScrollManager();

  @Override
  public @NonNull LineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new LineViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_log, parent, false), scrollManager);
  }

  @Override
  public void onBindViewHolder(@NonNull LineViewHolder holder, int position) {
    holder.bind(lines.get(position));
  }

  @Override
  public void onViewRecycled(@NonNull LineViewHolder holder) {
    holder.unbind();
  }

  @Override
  public int getItemCount() {
    return lines.size();
  }

  public void setLines(@NonNull List<LogLine> lines) {
    this.lines.clear();
    this.lines.addAll(lines);
    notifyDataSetChanged();
  }

  private static class ScrollManager {
    private final List<ScrollObserver> listeners = new CopyOnWriteArrayList<>();

    private int currentPosition;

    void subscribe(@NonNull ScrollObserver observer) {
      listeners.add(observer);
      observer.onScrollChanged(currentPosition);
    }

    void unsubscribe(@NonNull ScrollObserver observer) {
      listeners.remove(observer);
    }

    void notify(int position) {
      currentPosition = position;

      for (ScrollObserver listener : listeners) {
        listener.onScrollChanged(position);
      }
    }
  }

  private interface ScrollObserver {
    void onScrollChanged(int position);
  }

  static class LineViewHolder extends RecyclerView.ViewHolder implements ScrollObserver {

    private final ScrollManager        scrollManager;
    private final HorizontalScrollView scrollView;
    private final TextView             textView;

    public LineViewHolder(@NonNull View itemView, @NonNull ScrollManager scrollManager) {
      super(itemView);
      this.scrollManager = scrollManager;
      this.scrollView    = (HorizontalScrollView) itemView;
      this.textView      = itemView.findViewById(R.id.log_item_text);
    }

    @SuppressLint("NewApi")
    void bind(@NonNull LogLine line) {
      textView.setText(line.getBody());

      switch (line.getLogLevel()) {
        case NONE:    textView.setTextColor(Color.parseColor("#ffffff")); break;
        case VERBOSE: textView.setTextColor(Color.parseColor("#8a8a8a")); break;
        case DEBUG:   textView.setTextColor(Color.parseColor("#5ca72b")); break;
        case INFO:    textView.setTextColor(Color.parseColor("#46bbb9")); break;
        case WARNING: textView.setTextColor(Color.parseColor("#cdd637")); break;
        case ERROR:   textView.setTextColor(Color.parseColor("#ff6b68")); break;
      }

      scrollView.setOnScrollChangeListener((view, left, top, oldLeft, oldTop) -> {
        if (oldLeft - left != 0) {
          scrollManager.notify(left);
        }
      });

      scrollManager.subscribe(this);
    }

    void unbind() {
      scrollManager.unsubscribe(this);
    }

    @Override
    public void onScrollChanged(int position) {
      scrollView.scrollTo(position, 0);
    }
  }
}
