package com.greysonparrelli.debuglogsviewer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private LogLineAdapter lineAdapter;
    private MainViewModel  viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView status = findViewById(R.id.status);
        String   url    = getIntent() != null ? getIntent().getDataString() : null;

        if (TextUtils.isEmpty(url)) {
            status.setText(R.string.no_url);
        } else {
            status.setText(R.string.loading);
        }

        lineAdapter = new LogLineAdapter();

        RecyclerView logList = findViewById(R.id.list);
        logList.setAdapter(lineAdapter);
        logList.setLayoutManager(new LinearLayoutManager(this));

        viewModel = ViewModelProviders.of(this, new MainViewModel.Factory(new MainRepository(), url)).get(MainViewModel.class);
        viewModel.getLines().observe(this, lines -> {
            if (lines == null) return;

            status.setVisibility(View.GONE);
            lineAdapter.setLines(lines);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchViewItem = menu.findItem(R.id.search);

        final SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setIconifiedByDefault(false);

        if (!TextUtils.isEmpty(viewModel.getQuery())) {
            searchViewItem.expandActionView();
            searchView.setQuery(viewModel.getQuery(), true);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String query) {
                viewModel.filterByQuery(query);
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                viewModel.filterByQuery(query);
                searchView.clearFocus();
                return true;
            }
        });

        return true;
    }
}
