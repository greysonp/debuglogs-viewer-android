package com.greysonparrelli.debuglogsviewer;

import android.util.Log;

import androidx.annotation.NonNull;

import com.annimon.stream.Stream;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainRepository {

  private static final Pattern LOGGER_PATTERN = Pattern.compile("(.*) ([VDIWE]) ([^:]+): (.*)");

  private final OkHttpClient client;

  public MainRepository() {
    this.client = new OkHttpClient.Builder().build();
  }

  public void getLogs(@NonNull String url, @NonNull SuccessCallback success, @NonNull FailureCallback failure) {
    Request request = new Request.Builder().url(url).build();
    client.newCall(request).enqueue(new Callback() {
      @Override
      public void onFailure(@NonNull Call call, @NonNull IOException e) {
        failure.onFailure();
      }

      @Override
      public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
        if (response.isSuccessful() && response.body() != null) {
          success.onSuccess(parseLogs(response.body().string()));
        } else {
          failure.onFailure();
        }
      }
    });
  }

  private static List<LogLine> parseLogs(@NonNull String raw) {
    String[] lines = raw.split("\\n");

    int maxLength = Stream.of(lines).reduce(0, (currentMax, line) -> Math.max(currentMax, line.length()));

    return Stream.of(lines).map(line -> {
      Matcher loggerMatcher = LOGGER_PATTERN.matcher(line);

      if (loggerMatcher.matches()) {
        String level = loggerMatcher.group(2);
        switch (level.toLowerCase()) {
          case "v": return new LogLine(padRight(line, maxLength), LogLine.LogLevel.VERBOSE);
          case "d": return new LogLine(padRight(line, maxLength), LogLine.LogLevel.DEBUG);
          case "i": return new LogLine(padRight(line, maxLength), LogLine.LogLevel.INFO);
          case "w": return new LogLine(padRight(line, maxLength), LogLine.LogLevel.WARNING);
          case "e": return new LogLine(padRight(line, maxLength), LogLine.LogLevel.ERROR);
          default:  return new LogLine(padRight(line, maxLength), LogLine.LogLevel.NONE);
        }
      } else {
        return new LogLine(padRight(line, maxLength), LogLine.LogLevel.NONE);
      }
    }).toList();
  }

  public static String padRight(String s, int n) {
    return String.format("%-" + n + "s", s);
  }

  public interface SuccessCallback {
    void onSuccess(List<LogLine> lines);
  }

  public interface FailureCallback {
    void onFailure();
  }
}
