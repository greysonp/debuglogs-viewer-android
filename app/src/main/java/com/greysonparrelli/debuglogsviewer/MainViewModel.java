package com.greysonparrelli.debuglogsviewer;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;

class MainViewModel extends ViewModel {

  private static final String TAG = MainViewModel.class.getSimpleName();

  private final MainRepository                 repository;
  private final String                         url;
  private final MutableLiveData<List<LogLine>> lines;

  private List<LogLine> originalData;
  private String        query;

  private MainViewModel(@NonNull MainRepository repository, @Nullable String url) {
    this.repository   = repository;
    this.url          = url;
    this.lines        = new MutableLiveData<>();
    this.originalData = new ArrayList<>();
    this.query        = "";

    if (url != null) {
      repository.getLogs(url, result -> {
        originalData = result;
        lines.postValue(result);
      }, () -> Log.e(TAG, "We had a problem."));
    } else {
      Log.e(TAG, "Shrug.");
    }
  }

  @NonNull LiveData<List<LogLine>> getLines() {
    return lines;
  }

  void filterByQuery(@NonNull String query) {
    if (originalData.isEmpty()) {
      return;
    }

    this.query = query;

    String formattedQuery = query.toLowerCase();

    if (TextUtils.isEmpty(query)) {
      lines.postValue(originalData);
    } else {
      AsyncTask.SERIAL_EXECUTOR.execute(() -> {
        lines.postValue(Stream.of(originalData).filter(line -> line.getBody().toLowerCase().contains(formattedQuery)).toList());
      });
    }
  }

  @NonNull String getQuery() {
    return query;
  }

  public static class Factory extends ViewModelProvider.NewInstanceFactory {

    private final MainRepository repository;
    private final String         url;

    public Factory(@NonNull MainRepository repository, @Nullable String url) {
      this.repository = repository;
      this.url        = url;
    }

    @Override
    public @NonNull <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
      //noinspection ConstantConditions
      return modelClass.cast(new MainViewModel(repository, url));
    }
  }
}
